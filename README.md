Very simple bitbucket POST git hook endpoint. Use like this for example:

    require('bitbucket-githook')(function onPush(payload){...}, 9001, ['your private key here'], 'AUTODEPLOY')
    
Then register http git hook in your repo setting on bitbucket to your adress using the same key as you provided in parameter

    example-adress.com:9001/?key=yourkey

Any commit containing 'AUTODEPLOY' string in the message will trigger onPush function in your code.